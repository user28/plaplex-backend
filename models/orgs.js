'use strict'

const bcrypt = require('bcryptjs')

module.exports = (sequelize, DataTypes) => {
  const Orgs = sequelize.define('Orgs', {
    name: DataTypes.STRING,
    cityId: DataTypes.INTEGER,
    avatarId: DataTypes.INTEGER,
    description: DataTypes.TEXT,
    backgroundId: DataTypes.INTEGER,
    typeId: DataTypes.INTEGER,
    phone: DataTypes.STRING,
    email: DataTypes.STRING,
    inn: DataTypes.STRING,
    bill: DataTypes.STRING,
    password: DataTypes.STRING,
    role: {
      type: DataTypes.VIRTUAL,
      get() {
        return 'org'
      }
    }
  }, {})
  
  Orgs.associate = function (models) {
    Orgs.belongsTo(models.Cities)
    
    Orgs.hasOne(models.Attachments, {
      foreignKey: 'id',
      sourceKey: 'avatarId',
      as: 'avatar'
    })
    
    Orgs.hasOne(models.Attachments, {
      foreignKey: 'id',
      sourceKey: 'backgroundId',
      as: 'background'
    })
  }
  
  Orgs.prototype.comparePassword = (password, hash) => {
    return bcrypt.compareSync(password, hash)
  }
  
  Orgs.prototype.findOrgByEmail = (email) => {
    Orgs.findOne({
      where: {
        email: email
      }
    })
    .then(org => {
      return org
    })
  }
  
  Orgs.beforeSave((orgs, options) => {
    const hashPassword = (org) => {
      if (org.changed('password')) {
        const salt = bcrypt.genSaltSync(10)
        org.password = bcrypt.hashSync(org.password, salt)
      }
    }

    if (Array.isArray(orgs)) {
      orgs.map( hashPassword )
    } else {
      hashPassword(orgs)
    }
  })
  
  return Orgs
}

const setSaltAndPassword = org => {
  const salt = bcrypt.genSaltSync(10)
  
  if (org.password) {
    org.password = bcrypt.hashSync(org.password, salt)
  }
  
  if (org.attributes && org.attributes.password) {
    org.attributes.password = bcrypt.hashSync(org.attributes.password, salt)
  }
}
