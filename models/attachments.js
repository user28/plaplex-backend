'use strict'
const md5 = require('md5')
const path = require('path')

module.exports = (sequelize, DataTypes) => {
  const Attachments = sequelize.define('Attachments', {
    name:     DataTypes.STRING,
    mimetype: DataTypes.STRING,
    path:     DataTypes.STRING,
    link:     DataTypes.STRING
  }, {})

  Attachments.associate = function (models) {
    // associations can be defined here
  }

  Attachments.prototype.generateLink = (attachments) => {
    return `${ attachments.path }${ md5(attachments.name) }${ path.extname(attachments.name) }`
  }

  return Attachments
}
