'use strict'

const Sequelize = require('sequelize')

module.exports = (sequelize, DataTypes) => {
  const Comment_Marks = sequelize.define('Comment_Marks', {
    userId:    Sequelize.BIGINT,
    commentId: Sequelize.BIGINT,
    mark:      Sequelize.INTEGER,
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE
  }, {})
  Comment_Marks.associate = function (models) {
    Comment_Marks.belongsTo(models.Users)
    Comment_Marks.belongsTo(models.Comments)
  }
  return Comment_Marks
}