'use strict'
module.exports = (sequelize, DataTypes) => {
    const Excursion_Dates = sequelize.define('Excursion_Dates', {
        excursionId: DataTypes.INTEGER,
        date: DataTypes.DATE,
        price: DataTypes.INTEGER,
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: new Date()
        },
        updatedAt: DataTypes.DATE
    }, {})
    Excursion_Dates.associate = function (models) {
        Excursion_Dates.belongsTo(models.Excursions, {as: 'dates', foreignKey: 'id'})
    }
    return Excursion_Dates
}
