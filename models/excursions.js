'use strict'
const { Excursions, Excursion_Dates, Orgs, Attachments, Excursion_Attachments, Excursion_Tags, Tags } = require('../models/index.js')

module.exports = (sequelize, DataTypes) => {
  const Excursions = sequelize.define('Excursions', {
    name:                     DataTypes.STRING,
    description:              DataTypes.TEXT,
    imageId:                    DataTypes.INTEGER,
    price:                    DataTypes.INTEGER,
    shortDescription:         DataTypes.STRING,
    orgId:                    DataTypes.INTEGER,
    averageMark:              DataTypes.DECIMAL,
    participantsMin:          DataTypes.INTEGER,
    participantsMax:          DataTypes.INTEGER,
    isParticipantsListNeeded: DataTypes.BOOLEAN,
    createdAt:                DataTypes.DATE,
    updatedAt:                DataTypes.DATE,
  }, {})

  Excursions.associate = function (models) {
    Excursions.hasOne(models.Orgs, {
      foreignKey: 'id',
      sourceKey:  'orgId',
      as: 'org'
    })
    Excursions.hasMany(models.Excursion_Dates, { foreignKey: 'excursionId', as: 'dates' })
    Excursions.hasOne(models.Attachments, {
      foreignKey: 'id',
      sourceKey:  'imageId',
      as: 'image'
    })
    Excursions.hasMany(models.Excursion_Attachments, {as: 'photos'})
    Excursions.belongsToMany(models.Tags, {through: models.Excursion_Tags, foreignKey: 'excursionId', as: 'tags'})
  }

  return Excursions
}