'use strict'
module.exports = (sequelize, DataTypes) => {
  const Org_Comment_Marks = sequelize.define('Org_Comment_Marks', {
    commentId: DataTypes.INTEGER,
    userId:    DataTypes.INTEGER,
    mark:      DataTypes.INTEGER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {})
  Org_Comment_Marks.associate = function (models) {
    Org_Comment_Marks.belongsTo(models.Org_Comments)
    Org_Comment_Marks.belongsTo(models.Users)
  }
  return Org_Comment_Marks
}
