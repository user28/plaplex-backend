'use strict'
module.exports = (sequelize, DataTypes) => {
  const Countries = sequelize.define('Countries', {
    name:      DataTypes.STRING,
    lat:       DataTypes.DECIMAL,
    lng:       DataTypes.DECIMAL,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {})
  Countries.associate = function (models) {
    // associations can be defined here
  }
  return Countries
}