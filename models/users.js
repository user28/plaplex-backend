'use strict'

const bcrypt = require('bcryptjs')

module.exports = (sequelize, DataTypes) => {
    const Users = sequelize.define('Users', {
        cityId: DataTypes.INTEGER,
        email: DataTypes.STRING,
        phone: DataTypes.STRING,
        name: DataTypes.STRING,
        avatarId: DataTypes.STRING,
        level: DataTypes.STRING,
        password: {
            type: DataTypes.STRING,
            set(value) {
                return this.setDataValue('password', hashPassword(value))
            }
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
        role: {
            type: DataTypes.VIRTUAL,
            get() {
                return 'user'
            }
        }
    }, {})
    Users.associate = function (models) {
        Users.belongsTo(models.Cities)

        Users.hasOne(models.Attachments, {
            foreignKey: 'id',
            sourceKey: 'avatarId',
            as: 'avatar'
        })
    }

    Users.prototype.comparePassword = (password, hash) => {
        return bcrypt.compareSync(password, hash)
    }

    Users.prototype.findUserByEmail = (email) => {
        Users.findOne({
            where: {email: email}
        })
            .then(user => {
                return user
            })
    }

    return Users
}

const hashPassword = (password) => {
    const salt = bcrypt.genSaltSync(10)
    return bcrypt.hashSync(password, salt)
}
