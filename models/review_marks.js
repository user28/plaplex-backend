'use strict'
module.exports = (sequelize, DataTypes) => {
  const Review_Marks = sequelize.define('Review_Marks', {
    reviewId:  DataTypes.INTEGER,
    userId:    DataTypes.INTEGER,
    mark:      DataTypes.INTEGER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {})
  Review_Marks.associate = function (models) {
    Review_Marks.belongsTo(models.Reviews)
    Review_Marks.belongsTo(models.Users)
  }
  return Review_Marks
}
