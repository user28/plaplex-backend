'use strict'
module.exports = (sequelize, DataTypes) => {
  const Excursion_Tags = sequelize.define('Excursion_Tags', {
    excursionId: DataTypes.INTEGER,
    tagId:       DataTypes.INTEGER,
    createdAt:   DataTypes.DATE,
    updatedAt:   DataTypes.DATE
  }, {})
  
  return Excursion_Tags
}
