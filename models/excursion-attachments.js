'use strict'
module.exports = (sequelize, DataTypes) => {
  const Excursion_Attachments = sequelize.define('Excursion_Attachments', {
    excursionId: DataTypes.BIGINT(11),
    attachmentId: DataTypes.BIGINT(11)
  }, {})
  
  Excursion_Attachments.associate = function (models) {
    Excursion_Attachments.belongsTo(models.Attachments, { as: 'image', foreignKey: 'id' })
    Excursion_Attachments.belongsTo(models.Excursions, { as: 'excursion', foreignKey: 'id' })
  }
  
  return Excursion_Attachments
}