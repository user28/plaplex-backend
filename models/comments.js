'use strict'

const Sequelize = require('sequelize')

module.exports = (sequelize, DataTypes) => {
  const Comments = sequelize.define('Comments', {
    userId:    Sequelize.BIGINT,
    reviewId:  Sequelize.BIGINT,
    commentId: Sequelize.BIGINT,
    comment:   Sequelize.TEXT,
    photos:    Sequelize.JSON,
    rating:    Sequelize.DECIMAL(10),
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE
  }, {})
  Comments.associate = function (models) {
    Comments.belongsTo(models.Users)
    Comments.belongsTo(models.Reviews)
  }
  return Comments
}