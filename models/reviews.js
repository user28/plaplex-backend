'use strict'
module.exports = (sequelize, DataTypes) => {
  const Reviews = sequelize.define('Reviews', {
    userId:        DataTypes.INTEGER,
    excursionId:   DataTypes.INTEGER,
    excursionRate: DataTypes.DECIMAL,
    isText:        DataTypes.BOOLEAN,
    review:        DataTypes.TEXT,
    photos:        DataTypes.JSON,
    rating:        DataTypes.INTEGER,
    createdAt:     DataTypes.DATE,
    updatedAt:     DataTypes.DATE
  }, {})
  
  Reviews.associate = function (models) {
    Reviews.hasOne(models.Users, { as: 'user', foreignKey: 'id', sourceKey: 'userId' })
    Reviews.hasOne(models.Excursions, { as: 'excursion', foreignKey: 'id', sourceKey: 'excursionId' })
  }
  
  return Reviews
}
