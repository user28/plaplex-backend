'use strict'

const Sequelize = require('sequelize')

module.exports = (sequelize, DataTypes) => {

  const Cities = sequelize.define('Cities', {
    name:      Sequelize.STRING,
    countryId: Sequelize.BIGINT,
    lat:       Sequelize.DECIMAL(10),
    lng:       Sequelize.DECIMAL(10),
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE
  }, {})

  Cities.associate = function (models) {
    Cities.belongsTo(models.Countries)
  }

  return Cities
}