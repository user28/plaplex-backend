'use strict'
module.exports = (sequelize, DataTypes) => {
  const Tags = sequelize.define('Tags', {
    name:      DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {})
  
  Tags.associate = function (models) {
    Tags.belongsToMany(models.Excursions, {through: models.Excursion_Tags, foreignKey: 'tagId', as: 'excursions'})
  }
  
  return Tags
}