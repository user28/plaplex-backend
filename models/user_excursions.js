'use strict'
module.exports = (sequelize, DataTypes) => {
    const User_Excursions = sequelize.define('User_Excursions', {
        userId: DataTypes.INTEGER,
        excursionId: DataTypes.INTEGER,
        dateId: DataTypes.INTEGER,
        mark: DataTypes.DECIMAL,
        participants: DataTypes.INTEGER,
        paid: DataTypes.TINYINT(1),
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE
    }, {})

    User_Excursions.associate = function (models) {
        User_Excursions.hasOne(models.Users, {as: 'user', foreignKey: 'id', sourceKey: 'userId'})
        User_Excursions.hasOne(models.Excursions, {as: 'excursion', foreignKey: 'id', sourceKey: 'excursionId'})
        User_Excursions.hasOne(models.Excursion_Dates, {as: 'date', foreignKey: 'id', sourceKey: 'dateId'})
    }

    return User_Excursions
}
