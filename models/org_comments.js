'use strict'
module.exports = (sequelize, DataTypes) => {
  const Org_Comments = sequelize.define('Org_Comments', {
    orgId:     DataTypes.INTEGER,
    commentId: DataTypes.INTEGER,
    reviewId:  DataTypes.INTEGER,
    comment:   DataTypes.TEXT,
    photos:    DataTypes.JSON,
    rating:    DataTypes.DECIMAL,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {})
  Org_Comments.associate = function (models) {
    Org_Comments.belongsTo(models.Orgs)
    Org_Comments.belongsTo(models.Org_Comments)
    Org_Comments.belongsTo(models.Reviews)
  }
  return Org_Comments
}
