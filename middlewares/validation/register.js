const { Users, Orgs } = require('../../models/index')

module.export = async function register(email) {
  const userExists = await Users.findUserByEmail(email)
  const orgExists = await Orgs.findOrgByEmail(email)

  console.log(userExists)
  console.log(orgExists)

  if (userExists || orgExists) {
    return Promise.reject('Пользователь с такой электронной почтой уже зарегистрирован')
  }

  return Promise.resolve()
}
