const express = require('express')
const router = express.Router()
const Sequelize = require('sequelize')
const Op = Sequelize.Op

const passport = require('passport')
require('../config/passport.js')(passport)

const { Excursions, Excursion_Dates, Orgs, Attachments, Excursion_Attachments, Excursion_Tags, Tags, Reviews, Users } = require('../models/index.js')

router.get('/', async (req, res, next) => {
  const { excursionId } = req.query;
  
  Reviews.findAll({
      where: {
        excursionId: excursionId
      },
      include: [
        {
          model: Users,
          attributes: ['id', 'email', 'name'],
          include: {
            model: Attachments,
            as: 'avatar_att'
          },
          as: 'user'
        }
      ]
    }
  ).then(reviews => {
    return res.json({
      success: true,
      reviews
    })
  }).catch(error => {
    return res.json({
      success: false,
      error
    })
  })
})

router.post('/add', async (req, res, next) => {
  
  const { text, excursionId } = req.body;
  
  Reviews.create({
      userId: 1,
      excursionId: excursionId,
      isText: true,
      review: text
    }).then(async value => {
      const review = await Reviews.findByPk(value.id, {
        include: [
          {
            model: Users,
            attributes: ['id', 'email', 'name'],
            include: {
              model: Attachments,
              as: 'avatar_att'
            },
            as: 'user'
          }
        ]
      })
    return res.json({
      success: true,
      review
    })
  }).catch(error => {
    return res.json({
      success: false,
      error
    })
  })
})

module.exports = router
