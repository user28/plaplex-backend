const express = require('express')
const multer = require('multer')
const md5 = require('md5')
const path = require('path')
const router = express.Router()

const passport = require('passport')
require('../config/passport.js')(passport)

const {Attachments} = require('../models/index.js')

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/upload/')
    },
    filename: function (req, file, cb) {
        cb(null, `${md5(file.originalname + new Date())}${path.extname(file.originalname)}`)
    }
})

const upload = multer({storage: storage})

const backgroundUpload = async (req, res, next) => {
    try {
        const file = req.file

        Attachments.create({
            name: file.originalname,
            mimetype: file.mimetype,
            path: file.path,
            link: `http://localhost:9000/upload/${file.filename}`
        })
            .then(attachment => {
                return res.json({
                    success: true,
                    attachment
                })
            })
    } catch (e) {
        console.log(e)
        return res.status(500)
            .json({
                success: false,
                error: e
            })
    }
}

router.post('/upload', upload.single('logo'), async (req, res, next) => {
    try {

        const file = req.file

        Attachments.create({
            name: file.originalname,
            mimetype: file.mimetype,
            path: file.path,
            link: `http://localhost:9000/upload/${file.filename}`
        })
            .then(attachment => {
                return res.json({
                    success: true,
                    attachment
                })
            })
    } catch (e) {
        console.log(e)
        return res.status(500)
            .json({
                success: false,
                error: e
            })
    }
})

router.post('/upload/excursion-background', upload.single('file'), backgroundUpload)

module.exports = router
