const express = require('express')
const router = express.Router()
const Sequelize = require('sequelize')

const passport = require('passport')
require('../config/passport.js')(passport)

const {Excursions, Excursion_Dates, Orgs, Attachments, User_Excursions, Users} = require('../models/index.js')

const create = async (req, res, next) => {
    try {
        const {dateId, excursionId, participants, email, phone} = req.body
        const userId = req.user.id

        const excursion = await Excursions.findByPk(excursionId)

        const alreadyCount = await User_Excursions.sum('participants', {
            where: {
                excursionId: excursionId,
                dateId: dateId
            }
        })

        if (alreadyCount + participants > excursion.participantsMax) {
            return res.status(500)
                .json({
                    success: false,
                    error: 'Привышено максимальное число участников'
                })
        }

        User_Excursions.create({
            excursionId: excursionId,
            participants,
            userId: userId,
            dateId: dateId,
        }).then(data => {
            console.log(data);

            return res.json({
                success: true,
                data
            })
        })
    } catch (e) {
        console.log(e)
        return res.status(500)
            .json({
                success: false,
                error: e
            })
    }
}

const get = async (req, res, next) => {
    try {
        User_Excursions.findByPk(req.params.id, {
            include: [
                {
                    model: Excursions,
                    as: 'excursion'
                },
                {
                    model: Excursion_Dates,
                    as: 'date'
                }
            ]
        }).then(userExcursion => {
            return res.json({
                success: true,
                userExcursion
            })
        })
    } catch (e) {
        console.log(e)
        return res.status(500)
            .json({
                success: false,
                error: e
            })
    }
}

const update = async (req, res, next) => {
    try {
        const { id } = req.params;

        User_Excursions.update({paid: true}, {
            where: {
                id: id
            }
        }).then(_ => {
            return res.json({
                success: true
            })
        })
    } catch (e) {
        console.log(e)
        return res.status(500)
            .json({
                success: false,
                error: e
            })
    }
}

router.post('/check', passport.authenticate('jwt', {session: false}), async (req, res, next) => {
    try {
        const {date, excursionId, time} = req.body

        const count = await User_Excursions.sum('participants', {
            where: {
                excursionId: excursionId,
                dateId: date,
                time: time
            }
        })

        return res.json({
            success: true,
            count: count
        })
    } catch (e) {
        console.log(e)
        return res.status(500)
            .json({
                success: false,
                error: e
            })
    }
})

router.post('/create', passport.authenticate('jwt', {session: false}), create)
router.get('/:id', passport.authenticate('jwt', {session: false}), get)
router.put('/:id', passport.authenticate('jwt', {session: false}), update)

module.exports = router
