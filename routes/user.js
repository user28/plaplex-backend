const {body} = require('express-validator')

const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')

const config = require('./../config/configEnv.js')
const passport = require('passport')
require('../config/passport.js')(passport)

const {Users, Orgs} = require('../models/index.js')

const info = async (req, res, next) => {
    try {
        return res.json({
            success: true,
            user: req.user
        })
    } catch (error) {
        console.log(error)
        return res.status(500)
            .json({
                success: false,
                error: error
            })
    }
}

const generateJwt = function (id, email, role) {
    return jwt.sign({
        id: id,
        email: email,
        role: role
    }, config.secret, {
        expiresIn: config.TOKEN_EXPIRES_IN
    })
}

const create = async (req, res, next) => {
    try {
        const {email, name, password, passwordConfirmation} = req.body

        if (password !== passwordConfirmation) {
            return res.json({
                success: false,
                error: 'Пароли не совпадают'
            })
        }

        Users.create({
            email,
            name,
            password,
        })
            .then(user => {
                return res.json({
                    success: true,
                    token: generateJwt(user.id, email, 'user'),
                    user,
                    role: 'user'
                })
            })
    } catch (err) {
        console.log(err)
        return res.status(500)
            .json({
                success: false,
                error: err
            })
    }
}

router.post('/create', body('email')
    .custom(value => {
        return Users.findUserByEmail(value)
            .then(user => {
                if (user) {
                    return Promise.reject('Пользователь с такой электронной почтой уже зарегистрирован')
                }
            })
    }), create)

router.post('/login', async (req, res, next) => {
    const {email, password} = req.body

    const user = await Users.findOne({
        where: {
            email: email
        }
    })

    const org = await Orgs.findOne({
        where: {
            email: email
        }
    })

    if (user && user.comparePassword(password, user.password)) {
        return res.json({
            success: true,
            token: generateJwt(user.id, email, 'user'),
            user
        })
    } else if (org && org.comparePassword(password, org.password)) {
        return res.json({
            success: true,
            token: generateJwt(org.id, email, 'org'),
            user: org
        })
    } else {
        return res.json({
            success: false
        })
    }
})

router.put('/:id', passport.authenticate('jwt', {session: false}), async (req, res, next) => {
    try {
        const {name, email, phone, oldPassword, password} = req.body;
        if (oldPassword) {
            const user = await Users.findByPk(req.params.id);
            if (user && user.comparePassword(oldPassword, user.password))
                Users.update({password}, {
                    where: {
                        id: req.params.id
                    }
                })
                    .then(user => {
                        console.log(result)
                        return res.json({
                            success: true,
                            user
                        })
                    })
        } else {
            Users.update({name, email, phone}, {
                where: {
                    id: req.params.id
                }
            })
                .then(user => {
                    return res.json({
                        success: true,
                        user
                    })
                })
        }
    } catch (e) {
        console.log(e)
        return res.status(500)
            .json({
                success: false,
                error: e
            })
    }
})

router.get('/info', passport.authenticate('jwt', {session: false}), info)

router.get('/exists', async (req, res, next) => {
    try {
        const {email} = req.query

        const user = await Users.findOne({
            where: {
                email: email
            }
        })
        const org = await Orgs.findOne({
            where: {
                email: email
            }
        })

        if (user || org) {
            return res.json({
                success: true,
                exists: true
            })
        }

        return res.json({
            success: true,
            exists: false
        })
    } catch (error) {
        console.log(error)
        return res.status(500)
            .json({
                success: false,
                error: error
            })
    }
})

module.exports = router
