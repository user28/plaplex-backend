const express = require('express')
const router = express.Router()
const Sequelize = require('sequelize')
const Op = Sequelize.Op

const passport = require('passport')
require('../config/passport.js')(passport)

const {Excursions, Excursion_Dates, Orgs, Attachments, Excursion_Attachments, Excursion_Tags, Tags, User_Excursions} = require('../models/index.js')

const create = async (req, res, next) => {
    try {
        let { body:data } = req;
        let { image, dates } = data;

        delete data.image;
        delete data.dates;

        data.orgId = req.user.id;

        data = Object.assign(data, {imageId: image.id})

        Excursions.create(data).then(data => {
            const dateTimes = [];
            dates.map(date => {
                const { times } = date;
                return times.map(time => {
                    console.log(new Date(`${date.date} ${time.time}`))
                    console.log(`${date.date} ${time.time}`)
                    dateTimes.push({
                        excursionId: data.id,
                        date: new Date(`${date.date} ${time.time}`),
                        price: time.price
                    })
                });
            })
            Excursion_Dates.bulkCreate(dateTimes)
        }).catch(error => {
            console.log(error)
        })

        return res.json({success: true})
    } catch (e) {
        console.log(e)
        return res.status(500)
            .json({
                success: false,
                error: e
            })
    }
}

const recordUsersCount = async (req, res, next) => {
    try {
        const { query, params } = req;
        const { time } = query;
        const { id } = params;
        const count = await User_Excursions.sum('participants', {
            where: {
                excursionId: id,
                dateId: time
            }
        })

        return res.json({
            success: true,
            count: count
        })
    } catch (e) {
        console.log(e);
        return res.status(500)
            .json({
                success: false,
                error: e
            })
    }
}

router.get('/record-data/:id', passport.authenticate('jwt', {session: false}), recordUsersCount)

router.post('/create', passport.authenticate('jwt', {session: false}), create)

router.get('/recommended', async (req, res, next) => {
    try {
        const recommended = await Excursions.findAll({
            limit: 4,
            order: [
                ['createdAt', 'DESC']
            ],
            include: [
                {
                    model: Attachments,
                    as: 'image'
                },
                {
                    model: Orgs,
                    as: 'org'
                },
                {
                    model: Tags,
                    as: 'tags',
                },
                {
                    model: Excursion_Dates,
                    order: [
                        ['date', 'ASC']
                    ],
                    where: {
                        date: {
                            [Op.gte]: new Date()
                        }
                    },
                    as: 'dates'
                },
            ]
        })

        return res.json({
            success: true,
            recommended
        })
    } catch (e) {
        console.log(e)
        return res.status(500)
            .json({
                success: false,
                error: e
            })
    }
})

router.get('/list', async (req, res, next) => {
    try {
        const excursions = await Excursions.findAll({
            limit: 10,
            order: [
                ['createdAt', 'DESC']
            ],
            include: [
                {
                    model: Attachments,
                    as: 'image'
                }
            ]
        })

        return res.json({
            success: true,
            excursions
        })
    } catch (e) {
        console.log(e)
        return res.status(500)
            .json({
                success: false,
                error: e
            })
    }
})

router.get('/search', async (req, res, next) => {
    try {
        const {name, date_start, date_end, organisation, category} = req.query

        let category_query = {}
        let where = {}
        let date = {
            date: {
                [Op.gte]: new Date(),
            }
        }

        if (name) {
            where = {
                name: {
                    [Op.like]: `%${name}%`
                }
            }
        }

        if (date_start && date_end) {
            date = {
                date: {
                    [Op.and]: [
                        {
                            [Op.gte]: new Date(),
                        },
                        {
                            [Op.between]: [Date.parse(date_start), Date.parse(date_end)]
                        }
                    ]
                }
            }
        }

        if (organisation) {
            where = Object.assign(where, {orgId: organisation})
        }

        if (category) {
            category_query = {
                '$tags.id$': category
            }
        }

        const excursions = await Excursions.findAll({
            order: [
                ['createdAt', 'DESC']
            ],
            where: {
                ...where,
                ...category_query
            },
            include: [
                {
                    model: Attachments,
                    as: 'image'
                },
                {
                    model: Excursion_Dates,
                    where: {
                        ...date
                    },
                    limit: 2,
                    order: [
                        ['date', 'ASC']
                    ],
                    as: 'dates'
                },
                {
                    model: Tags,
                    as: 'tags',
                }
            ]
        })

        return res.json({
            success: true,
            excursions
        })
    } catch (e) {
        console.log(e)
        return res.status(500)
            .json({
                success: false,
                error: e
            })
    }
})

router.get('/:id', async (req, res, next) => {
    const {id} = req.params
    Excursions.findByPk(id, {
        include: [
            {
                model: Attachments,
                as: 'image'
            },
            {
                model: Excursion_Dates,
                order: [
                    ['date', 'ASC']
                ],
                as: 'dates'
            },
            {
                model: Orgs,
                attributes: ['id', 'name'],
                include: {
                    model: Attachments,
                    as: 'avatar'
                },
                as: 'org'
            },
            {
                model: Excursion_Attachments,
                as: 'photos',
                include: [
                    {
                        model: Attachments,
                        as: 'image'
                    }
                ]
            },
            {
                model: Tags,
                as: 'tags',
            }
        ]
    }).then(excursion => {
        if (excursion) {
            return res.json({
                success: true,
                excursion,
            })
        } else {
            return res.json({
                success: false,
                error: 'EXCURSION NOT FOUND'
            })
        }
    }).catch(error => {
        console.log(error)

        return res.json({
            success: false,
            error
        })
    })
})

module.exports = router
