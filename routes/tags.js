const { body } = require('express-validator')

const express = require('express')
const router = express.Router()

const passport = require('passport')
require('../config/passport.js')(passport)

const { Tags } = require('../models/index.js')

router.get('/', async (req, res, next) => {
  Tags.findAll({}).then(tags => {
    return res.json({
      success: true,
      tags
    })
  })
})

module.exports = router
