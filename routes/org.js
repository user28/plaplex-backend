const { body } = require('express-validator')

const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const Sequelize = require('sequelize')

const config = require('./../config/configEnv.js')
const passport = require('passport')
require('../config/passport.js')(passport)

const { Orgs, Attachments } = require('../models/index.js')

const generateJwt = function (id, email, role) {
  return jwt.sign({
    id: id,
    email: email,
    role: role
  }, config.secret, {
    expiresIn: config.TOKEN_EXPIRES_IN
  })
}

router.post('/create', async (req, res, next) => {
  const { email, name, passwordConfirmation, password, phone, inn, bill } = req.body
  
  if (password !== passwordConfirmation) {
    return res.json({
      success: false,
      error: 'Пароли не совпадают'
    })
  }
  
  const check = await Orgs.findOne({ where: { email: email } })
  
  if (check) {
    return res.json({
      success: false,
      error: 'Организация с такой электронной почтой зарегистрированна'
    })
  }
  
  const org = new Orgs({
    email,
    name,
    password,
    inn,
    bill
  })
  
  org.save().then(data => {
    return res.json({
      success: true,
      token: generateJwt(org.id, email, 'org'),
      role: 'org',
      user: data
    })
  }).catch(error => {
    console.log(error)
    return res.json({
      success: false,
      error
    })
  })
})

router.post('/login', async (req, res, next) => {
  const { email, password } = req.body
  
  Orgs.findOne({ where: { email: email } })
  .then(org => {
    if (org.comparePassword(password, org.password)) {
      return res.json({
        success: true,
        token: generateJwt(org.id, email),
        user: org,
        role: 1
      })
    }
  })
  .catch(error => {
    return res.status(500)
    .json({
      success: false,
      error: error.message
    })
  })
})

router.put('/:id', async (req, res, next) => {
  const { name, email, phone, inn, bill, avatar, oldPassword, password } = req.body
  if (oldPassword) {
    const org = await Orgs.findByPk(req.params.id)
    if (org && org.comparePassword(oldPassword, org.password)) {
      Orgs.update({ password }, {
        where: {
          id: req.params.id
        }
      }).then(_ => {
        return res.json({
          success: true
        })
      }).catch(error => {
        console.log(error)
        return res.json({
          success: false
        })
      })
    } else {
      return res.json({
        success: false,
        error: 'Старый пароль указан не верно'
      })
    }
  } else {
    Orgs.update({ name, email, phone, inn, bill }, {
      where: {
        id: req.params.id
      }
    })
    .then(user => {
      return res.json({
        success: true,
        user
      })
    }).catch(error => {
      return res.json({
        success: false,
        error
      })
    })
  }
})

router.get('/:id', async (req, res, next) => {
  try {
    const { id } = req.params
    const organisation = await Orgs.findByPk(id, {
      include: [
        {
          model: Attachments,
          as: 'avatar'
        },
        {
          model: Attachments,
          as: 'background'
        }
      ]
    })
    
    return res.json({
      success: true,
      organisation
    })
  } catch (error) {
    console.log(error)
    return res.status(500)
    .json({
      success: false,
      error: error
    })
  }
})

module.exports = router
