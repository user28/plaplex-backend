'use strict'
const bcrypt = require('bcryptjs')

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */

    return queryInterface.bulkInsert('Orgs', [{
      name:        'Музей истории шоколада «Криолло»',
      cityId:      1,
      avatarId:      1,
      description: 'Музей истории шоколада «Криолло» Музей истории шоколада «Криолло»',
      backgroundId:  2,
      typeId:      1,
      phone:       '+79229245806',
      email:       'yura.cheglakov@gmail.com',
      password:    password('541531'),
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name:        'Музей мороженого «Артико»',
      cityId:      1,
      avatarId:      2,
      description: 'Музей мороженого «Артико» Музей мороженого «Артико»',
      backgroundId:  3,
      typeId:      1,
      phone:       '+79229245806',
      email:       'yura.cheglakov.ya@yandex.ru',
      password:    password('541531'),
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name:        'Передержка собак "Мокрый нос" КРООЗЖ "Дари добро"',
      cityId:      1,
      avatarId:      3,
      description: 'Передержка собак "Мокрый нос" КРООЗЖ "Дари добро" Передержка собак "Мокрый нос" КРООЗЖ "Дари добро"',
      backgroundId:  4,
      typeId:      2,
      phone:       '+79229245806',
      email:       'yura.cheglakov.ya2@yandex.ru',
      password:    password('541531'),
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name:        'АО «ЛЕПСЕ»',
      cityId:      1,
      avatarId:      4,
      description: 'АО «ЛЕПСЕ» АО «ЛЕПСЕ»',
      backgroundId:  5,
      typeId:      3,
      phone:       '+79229245806',
      email:       'yura.cheglakov.ya3@yandex.ru',
      password:    password('541531'),
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name:        'Кировское областное государственное бюджетное учреждение культуры “Вятский художественный музей имени В. М. и А. М. Васнецовых”',
      cityId:      1,
      avatarId:      5,
      description: 'Кировское областное государственное бюджетное учреждение культуры “Вятский художественный музей имени В. М. и А. М. Васнецовых”',
      backgroundId:  6,
      typeId:      1,
      phone:       '+79229245806',
      email:       'yura.cheglakov.ya4@yandex.ru',
      password:    password('541531'),
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name:        'Иванов. И.И.',
      cityId:      1,
      avatarId:      6,
      description: 'Иванов. И.И.',
      backgroundId:  7,
      typeId:      1,
      phone:       '+79229245806',
      email:       'yura.cheglakov.ya5@yandex.ru',
      password:    password('541531'),
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name:        'Кировский музей железнодорожного транспорта',
      cityId:      1,
      avatarId:      7,
      description: 'Кировский музей железнодорожного транспорта',
      backgroundId:  1,
      typeId:      1,
      phone:       '+79229245806',
      email:       'yura.cheglakov.ya6@yandex.ru',
      password:    password('541531'),
      createdAt: new Date(),
      updatedAt: new Date()
    }], {})
  },

  down: (queryInterface, Sequelize) => {
    /*Кировский музей железнодорожного транспорта
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
}

const password = (password) => {
  const salt = bcrypt.genSaltSync(10)
  return bcrypt.hashSync(password, salt)
}
