'use strict';
const bcrypt = require('bcryptjs')

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
     Add altering commands here.
     Return a promise to correctly handle asynchronicity.
     
     Example:
     return queryInterface.bulkInsert('People', [{
     name: 'John Doe',
     isBetaMember: false
     }], {});
     */
    
    return queryInterface.bulkInsert('Users', [
      {
        cityId: 1,
        email: 'anonymous@anonymous.com',
        phone: '+79999999999',
        name: 'Аноним',
        avatarId: 1,
        level: 1,
        password: password('541531'),
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },
  
  down: (queryInterface, Sequelize) => {
    /*
     Add reverting commands here.
     Return a promise to correctly handle asynchronicity.
     
     Example:
     return queryInterface.bulkDelete('People', null, {});
     */
  }
};

const password = (password) => {
  const salt = bcrypt.genSaltSync(10)
  return bcrypt.hashSync(password, salt)
}
