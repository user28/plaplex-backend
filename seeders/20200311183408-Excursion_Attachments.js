'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
     Add altering commands here.
     Return a promise to correctly handle asynchronicity.
     
     Example:
     return queryInterface.bulkInsert('People', [{
     name: 'John Doe',
     isBetaMember: false
     }], {});
     */
    
    return queryInterface.bulkInsert('Excursion_Attachments', [
      {
        excursionId: 1,
        attachmentId: 1
      },
      {
        excursionId: 1,
        attachmentId: 2
      },
      {
        excursionId: 1,
        attachmentId: 3
      },
      {
        excursionId: 1,
        attachmentId: 4
      },
      {
        excursionId: 1,
        attachmentId: 5
      },
      {
        excursionId: 1,
        attachmentId: 6
      },
      {
        excursionId: 1,
        attachmentId: 7
      },
      {
        excursionId: 2,
        attachmentId: 1
      },
      {
        excursionId: 2,
        attachmentId: 2
      },
      {
        excursionId: 2,
        attachmentId: 3
      },
      {
        excursionId: 2,
        attachmentId: 3
      },
      {
        excursionId: 3,
        attachmentId: 4
      },
      {
        excursionId: 3,
        attachmentId: 5
      },
      {
        excursionId: 3,
        attachmentId: 6
      },
      {
        excursionId: 4,
        attachmentId: 7
      },
      {
        excursionId: 4,
        attachmentId: 1
      }
    ], {});
  },
  
  down: (queryInterface, Sequelize) => {
    /*
     Add reverting commands here.
     Return a promise to correctly handle asynchronicity.
     
     Example:
     return queryInterface.bulkDelete('People', null, {});
     */
  }
};
