'use strict'

module.exports = {
    up: (queryInterface, Sequelize) => {
        /*
          Add altering commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.bulkInsert('People', [{
            name: 'John Doe',
            isBetaMember: false
          }], {});
        */

        const records = [];

        for (let index = 1; index <= 7; index++) {
            const counts = Math.floor(Math.random() * 6) + 2;

            for (let count = 1; count <= counts; count++) {
                const record = {
                    excursionId: index,
                    date: randomDate(new Date(), new Date(2021, 0, 0), 9, 16),
                    price: 100,
                    createdAt: new Date(),
                    updatedAt: new Date()
                }

                records.push(record);
            }
        }

        return queryInterface.bulkInsert('Excursion_Dates', records)
    },

    down: (queryInterface, Sequelize) => {
        /*
          Add reverting commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.bulkDelete('People', null, {});
        */
    }
}

function randomDate(start, end, startHour, endHour) {
  const date = new Date(+ start + Math.random() * (end - start));
  const hour = startHour + Math.random() * (endHour - startHour) | 0;
  date.setHours(hour);
  return date;
}
