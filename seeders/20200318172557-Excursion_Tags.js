'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
     Add altering commands here.
     Return a promise to correctly handle asynchronicity.
     
     Example:
     return queryInterface.bulkInsert('People', [{
     name: 'John Doe',
     isBetaMember: false
     }], {});
     */
    
    return queryInterface.bulkInsert('Excursion_Tags', [
      {
        excursionId: 1,
        tagId: 1
      },
      {
        excursionId: 1,
        tagId: 2
      },
      {
        excursionId: 1,
        tagId: 3
      },
      {
        excursionId: 2,
        tagId: 1
      },
      {
        excursionId: 2,
        tagId: 3
      },
      {
        excursionId: 3,
        tagId: 2
      },
      {
        excursionId: 4,
        tagId: 1
      },
      {
        excursionId: 4,
        tagId: 2
      },
      {
        excursionId: 5,
        tagId: 2
      },
      {
        excursionId: 5,
        tagId: 3
      },
      {
        excursionId: 6,
        tagId: 2
      },
      {
        excursionId: 7,
        tagId: 3
      }
    ], {});
  },
  
  down: (queryInterface, Sequelize) => {
    /*
     Add reverting commands here.
     Return a promise to correctly handle asynchronicity.
     
     Example:
     return queryInterface.bulkDelete('People', null, {});
     */
  }
};
