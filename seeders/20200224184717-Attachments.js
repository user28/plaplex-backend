'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */

    return queryInterface.bulkInsert('Attachments', [{
      name:     'Image-1',
      mimetype: 'image/jpeg',
      path:     '/upload/1.jpg',
      link:     'http://localhost:9000/upload/1.jpg',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name:     'Image-2',
      mimetype: 'image/jpeg',
      path:     '/upload/2.jpg',
      link:     'http://localhost:9000/upload/2.jpg',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name:     'Image-3',
      mimetype: 'image/jpeg',
      path:     '/upload/3.jpg',
      link:     'http://localhost:9000/upload/3.jpg',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name:     'Image-4',
      mimetype: 'image/jpeg',
      path:     '/upload/4.jpg',
      link:     'http://localhost:9000/upload/4.jpg',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name:     'Image-5',
      mimetype: 'image/jpeg',
      path:     '/upload/5.jpg',
      link:     'http://localhost:9000/upload/5.jpg',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name:     'Image-6',
      mimetype: 'image/jpeg',
      path:     '/upload/6.jpg',
      link:     'http://localhost:9000/upload/6.jpg',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name:     'Image-7',
      mimetype: 'image/jpeg',
      path:     '/upload/7.jpg',
      link:     'http://localhost:9000/upload/7.jpg',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {})
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
}
