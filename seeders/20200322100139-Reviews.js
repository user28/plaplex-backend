'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
     Add altering commands here.
     Return a promise to correctly handle asynchronicity.
     
     Example:
     return queryInterface.bulkInsert('People', [{
     name: 'John Doe',
     isBetaMember: false
     }], {});
     */
    
    return queryInterface.bulkInsert('Reviews', [
      {
        userId: 1,
        excursionId: 1,
        excursionRate: 4,
        isText: true,
        review: 'Отличная экскурсия! Рекомендую',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        userId: 1,
        excursionId: 2,
        excursionRate: 4,
        isText: true,
        review: 'Отличная экскурсия! Рекомендую',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        userId: 1,
        excursionId: 3,
        excursionRate: 4,
        isText: true,
        review: 'Отличная экскурсия! Рекомендую',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        userId: 1,
        excursionId: 4,
        excursionRate: 4,
        isText: true,
        review: 'Отличная экскурсия! Рекомендую',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        userId: 1,
        excursionId: 5,
        excursionRate: 4,
        isText: true,
        review: 'Отличная экскурсия! Рекомендую',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        userId: 1,
        excursionId: 6,
        excursionRate: 4,
        isText: true,
        review: 'Отличная экскурсия! Рекомендую',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        userId: 1,
        excursionId: 7,
        excursionRate: 4,
        isText: true,
        review: 'Отличная экскурсия! Рекомендую',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },
  
  down: (queryInterface, Sequelize) => {
    /*
     Add reverting commands here.
     Return a promise to correctly handle asynchronicity.
     
     Example:
     return queryInterface.bulkDelete('People', null, {});
     */
  }
};
