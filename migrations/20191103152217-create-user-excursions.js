'use strict'
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('User_Excursions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT(11)
      },
      userId: {
        type: Sequelize.BIGINT(11),
        references: {
          model: 'Users',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      excursionId: {
        type: Sequelize.BIGINT(11),
        references: {
          model: 'Excursions',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      dateId: {
        type: Sequelize.BIGINT(11),
        references: {
          model: 'Excursion_Dates',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      mark: {
        type: Sequelize.DECIMAL(10)
      },
      participants: {
        type: Sequelize.Number
      },
      paid: {
        type: Sequelize.BOOL
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: new Date()
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: new Date()
      }
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('User_Excursions')
  }
}