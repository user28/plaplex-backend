'use strict'
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Excursion_Attachments', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT(11)
      },
      excursionId: {
        type: Sequelize.BIGINT(11),
        references: {
          model: 'Excursions',
          key: 'id'
        },
        onDelete: 'CASCADE'
      },
      attachmentId: {
        type: Sequelize.BIGINT(11),
        references: {
          model: 'Attachments',
          key: 'id'
        },
        onDelete: 'CASCADE'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: new Date()
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: new Date()
      }
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Excursion_Attachments')
  }
}