'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Excursions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT(11)
      },
      name: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.TEXT
      },
      imageId: {
        type: Sequelize.BIGINT(11),
        references: {
          model: 'Attachments',
          key: 'id'
        },
        onDelete: 'CASCADE'
      },
      price: {
        type: Sequelize.INTEGER
      },
      shortDescription: {
        type: Sequelize.STRING
      },
      orgId: {
        type: Sequelize.BIGINT(11),
        references: {
          model: 'Orgs',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      averageMark: {
        type: Sequelize.DECIMAL(10)
      },
      participantsMin: {
        type: Sequelize.INTEGER
      },
      participantsMax: {
        type: Sequelize.INTEGER
      },
      isParticipantsListNeeded: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: new Date()
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: new Date()
      }
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Excursions')
  }
}
