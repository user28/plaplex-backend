'use strict'
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Cities', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT(11)
      },
      name: {
        type: Sequelize.STRING
      },
      countryId: {
        type: Sequelize.BIGINT(11),
        references: {
          model: 'Countries',
          key: 'id'
        },
        
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      lat: {
        allowNull: false,
        type: Sequelize.DECIMAL(10)
      },
      lng: {
        allowNull: false,
        type: Sequelize.DECIMAL(10)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: new Date()
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: new Date()
      }
    })
  },
  
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Cities')
  }
}