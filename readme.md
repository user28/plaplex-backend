1. Склонировать репозиторий
2. Перейти в дерикторию с проектом
3. Запустить команду npm install
4. Настроить подключение в .env `DATABASE=mariadb://root:secret@localhost:3306/plaplex`

!!!Перед следующими шагами должна быть запущена БД (mariadb ^10.2)
5. Изменить в `seeders/20200224185548-Orgs.js` данные организаторов по необходимости
6. Запустить команду npx sequelize-cli db:migrate
7. Запустить команду npx sequelize-cli db:seed:all

Сервер запускается командой `npm run start`