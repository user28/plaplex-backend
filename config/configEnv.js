module.exports = {
  'secret':           process.env.SECRET_KEY,
  'database':         process.env.DATABASE,
  'TOKEN_EXPIRES_IN': process.env.TOKEN_EXPIRES_IN
}
