const JwtStrategy = require('passport-jwt').Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt
const Sequelize = require('sequelize')

// load up the user model
const { Users, Orgs, Attachments } = require('../models/index.js')
const config = require('../config/configEnv.js') // get main config file

module.exports = (passport) => {
  const opts = {}
  opts.jwtFromRequest = ExtractJwt.fromExtractors(
    [
      ExtractJwt.fromAuthHeaderAsBearerToken()
    ]
  )
  opts.secretOrKey = config.secret

  passport.use(new JwtStrategy(opts, async (jwt_payload, done) => {
    try {
      let user, org, anonymous = undefined
      if (jwt_payload.role === 'user') {
        user = await Users.findByPk(jwt_payload.id)
      }
      else if (jwt_payload.role === 'org') {
        org = await Orgs.findByPk(jwt_payload.id, {
          include: [
            {
              model: Attachments,
              as: 'avatar'
            },
            {
              model: Attachments,
              as: 'background'
            }
          ]
        })
      }
      else {
        anonymous = await Users.findByPk(1)
      }

      return done(null, user || org || anonymous)
    }
    catch (e) {
      return done(e)
    }
  }))
}
