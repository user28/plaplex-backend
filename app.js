const createError = require('http-errors')
const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const dotenv = require('dotenv')
dotenv.config()

const app = express()

// const config = require('./config/config');
const passport = require('passport')

app.use(passport.initialize())
app.use(express.json({ limit: '50mb' }))
app.use(express.urlencoded({
  limit:    '50mb',
  extended: false
}))
app.use(cookieParser())
app.use(bodyParser.json({ limit: '50mb' }))
app.use(bodyParser.urlencoded({
  limit:    '50mb',
  extended: false
}))

app.use(express.static(path.join(__dirname, 'public')))

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')
  next()
})

app.use('/api/user', require('./routes/user'))
app.use('/api/org', require('./routes/org'))
app.use('/api/excursion', require('./routes/excursion'))
app.use('/api/attachment', require('./routes/attachment'))
app.use('/api/user-excursions', require('./routes/user-excursions'))
app.use('/api/tags', require('./routes/tags'))
app.use('/api/review', require('./routes/reviews'))

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404))
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)

  console.log(err)

  return res.json({
    error:   err.message,
    success: false
  })
})

module.exports = app
